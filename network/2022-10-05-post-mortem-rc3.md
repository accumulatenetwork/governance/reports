# Post-mortem of issues and challenges on the RC3 testnet

## Preserving RC2 account data

This was not the best idea. RC2 had a far higher faucet amount and an infinite
token pool and tokens fauceted on RC2 were preserved and added to the ACME
issued amount. This lead to an amount of ACME issued higher than the supply
limit.

Also, RC2 had two BVNs and RC3 had three, so we merged and split the data set.
However, the logic of which signature gets recorded where is complex and not
something I wanted to replicate in splitting the data set, so my solution was
less than ideal.

## Performance

Syncing a node is much slower than we'd like. Profiles showed a lot of time
spent in Sentry error handling and ED25519 signature validation. I decided
Sentry is a bad fit and we're not really using it so I removed it (AC-3209). And
many of the signatures were from followers and discarded, so I reordered a
couple steps to fail earlier to avoid some ED25519 signature validation
(AC-3208).

Syncing a node still has odd performance behavior. Restarting the node often
considerably improves block times (during sync/replay). This indicates there's
something we can improve, even with the changes I made. This needs to be
investigated.

## Peering

Tendermint is supposed to know how to determine the node's public address so it
can advertize that address (AC-3218). Tendermint's logic was not working as
advertized, so most of the nodes were advertizing unroutable addresses (e.g.
`0.0.0.0`). This was fixed by updating the initialization process to explicitly
set the external address advertized by Tendermint.

## Snapshot bugs

Snapshot headers were not being written correctly (AC-3210), and restoring to a
snapshot caused a panic (also fixed in AC-3210). We need more tests to prevent
regressions like this in the future.

## Anchoring

### Healing

Now that anchors must be signed by 2/3 of validators, the builtin anchor healing
is insufficient. I wrote a tool that scans the anchor ledgers for pending
anchors and fetches and resends signatures to resolve the pending anchors. We
need an automated system to check for pending anchors and run the tool if
needed.

Long term, we may be able to fix the builtin healing. Because of the
architecture of anchoring, healing requires querying nodes individually, which
requires knowing the address of nodes. If nodes have their API exposed and can
discover every other node through peering, healing can scan peer lists to find
nodes to query them for signatures. Otherwise, we'll need a different solution.
If we build a peer-to-peer comms layer (see DO-47) that every node participates
in, healing can use that.

### Inappropriate anchoring

A node determined if it was a validator or follower by looking at its config
(AC-3220). If a node thought it was a validator, it would send out anchors.
Thus, when an operator boots up a node that will be a validator and that node
has to catch up, the node is processing thousands of blocks of transactions and
it thinks its a validator, so it sends out anchors. This was fixed by instead
determining "am I a validator" by comparing the node's validator key to the
current validator set (at the current block height). With this change, validator
nodes will no longer send out anchors until they reach the block height where
they are activated (and new nodes waren't be activated until after they catch
up).

Compounding this, the ABCI's CheckTx logic would consider anchors and synthetic
transactions valid even if the executor returned "that transaction has already
been delivered" (AC-3227). Because of this, old, already processed anchors sent
out by validators catching up were allowed into the mempool and into blocks.
This is bad because those anchors were guaranteed to fail with "that transaction
has already been delivered," so they caused entirely pointless extra load.

As a result, the mempools were constantly full of mostly useless anchors. This
made it difficult to get anything else through, including anchors for new
blocks.

## Synchronizing via snapshot

Synchronizing to a snapshot was affected by a number of the issues above. Once
those were solved or worked around, it worked, for a time. However, snapshot
synchronization appeared to be buggy, so I told operators to restart after the
initial synchronization. After the fact, I learned that the node appeared to be
doing nothing because it was backfilling light blocks (this is a Tendermint
thing) and restarting it terminated the backfilling process.

Operators that brought their nodes online later were unable to use snapshots to
syncrhonize. This was caused at least in part by terminating the backfilling
process. To make problems worse, no other snapshots were being captured, because
snapshot capturing is driven by anchoring, and anchoring was essentially
permanently stalled by the mempools being constantly full.
